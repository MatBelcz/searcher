'use strict';

angular.module('uiApp').constant('CONSTANTS', {
    BASE_API_URL: 'http://localhost:5000/api/',
    SEARCH_TASK_ROW_COLORS: {
        SUCCESS: 'label label-success',
        FAILURE: 'label label-danger',
        STARTED: 'label label-info',
        PENDING: 'label label-default'
    },
    AUTH: {
        LOGIN: 'http://localhost:5000/api/auth/login',
        REGISTER: 'http://localhost:5000/api/auth/register',
        LOGOUT: 'http://localhost:5000/api/auth/logout'
    },
    SEARCH: 'http://localhost:5000/api/search/'
});
