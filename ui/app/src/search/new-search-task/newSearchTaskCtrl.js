'use strict';

angular.module('uiApp').controller('NewSearchTaskCtrl', ['SearchService', '$location', NewSearchTaskCtrl]);

function NewSearchTaskCtrl(SearchService, $location){
    var vm = this;
    vm.model = {
        searchKeys: '',
        file: null
    };
    vm.errors = [];

    vm.startSearchTask = function(){
        var formData = new FormData();

        formData.set('file', vm.model.file);
        formData.set('searchKeys', vm.model.searchKeys);

        SearchService.startSearch(formData).then(
            function(response){
                $location.path('/');
        },
            function(response){
                vm.errors = response.data.errors;
        },
            function(){});
    };
}
