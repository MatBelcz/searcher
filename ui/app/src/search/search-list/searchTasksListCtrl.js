'use strict';

angular.module('uiApp').controller('SearchTasksListCtrl', ['SearchService', 'CONSTANTS', '$uibModal', SearchTasksListCtrl]);

function SearchTasksListCtrl(SearchService, CONSTANTS, $uibModal){
    var vm = this;
    vm.loader = true;
    vm.data = [];
    vm.colors = CONSTANTS.SEARCH_TASK_ROW_COLORS;

    SearchService.getSearchTasks().then(
        function(response){
            vm.data = response.data;
        }, function(error){

    }).finally(function(){
        vm.loader = false;
    });

    vm.showResults = function(taskData){
         var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'src/search/search-list/search-result.html',
            controller: function($scope) {
                $scope.taskData = taskData;
                $scope.close = modalInstance.close;
             },
            size: 'lg'
        });
    };

    vm.isReadyToOpen = function(searchTaskStatus){
        return searchTaskStatus === 'FAILURE' || searchTaskStatus === 'SUCCESS';
    }

}
