'use strict';

angular.module('uiApp').factory('SearchService', ['$http', 'CONSTANTS', 'AuthService', SearchService]);

function SearchService($http, CONSTANTS, AuthService) {
    var service = {};


    service.getSearchTasks = function () {
        var headers = AuthService.getAuthHeader();
        headers['Content-Type'] = 'application/json';

        return $http({
            method: 'GET',
            url: CONSTANTS.BASE_API_URL + 'search/',
            headers: headers
        });
    };

    service.startSearch = function (formData) {
        var headers = AuthService.getAuthHeader();
        headers['Content-Type'] = undefined;

        return $http.post(CONSTANTS.BASE_API_URL + 'search/', formData, {
            transformRequest: angular.identity,
            headers: headers
        });
    };

    return service;
}