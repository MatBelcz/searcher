'use strict';

angular.module('uiApp').factory('AuthService', ['$http', 'CONSTANTS', '$window', '$q', AuthService]);

function AuthService($http, CONSTANTS, $window, $q) {
    var service = {};

    service.getUserInfo = function () {
        var userInfo = $window.sessionStorage.getItem(CONSTANTS.USER_INFO);

        if (userInfo === null) return userInfo;

        return JSON.parse(userInfo);
    };

    service.getAuthHeader = function () {
        var userInfo = service.getUserInfo();

        return {'Authorization': 'Bearer ' + userInfo.authToken};
    };

    service.register = function (data) {
        var deferred = $q.defer();

        $http.post(CONSTANTS.AUTH.REGISTER, data).then(function (result) {
            var userInfo = {
                authToken: result.data.authToken,
                email: data.email
            };

            $window.sessionStorage[CONSTANTS.USER_INFO] = JSON.stringify(userInfo);
            deferred.resolve(userInfo);

        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    service.login = function (data) {
        var deferred = $q.defer();

        $http.post(CONSTANTS.AUTH.LOGIN, data).then(function (result) {
            var userInfo = {
                authToken: result.data.authToken,
                email: data.email
            };

            $window.sessionStorage[CONSTANTS.USER_INFO] = JSON.stringify(userInfo);
            deferred.resolve(userInfo);

        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    service.logout = function () {
        var deferred = $q.defer();

        $http.post(CONSTANTS.AUTH.LOGOUT, {}, {headers: service.getAuthHeader()})
            .then(
                function (response) {
                    service.unsetAuthToken();
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });

        return deferred.promise;
    };

    service.removeAuthToken = function () {
        $window.sessionStorage.removeItem(CONSTANTS.USER_INFO);
    };

    service.isLoggedIn = function () {
        return $window.sessionStorage.getItem(CONSTANTS.USER_INFO) !== null
    };

    service.unsetAuthToken = function () {
        return $window.sessionStorage.removeItem(CONSTANTS.USER_INFO);
    };

    return service;
}