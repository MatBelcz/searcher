'use strict';

angular.module('uiApp').controller('MenuCtrl', ['AuthService', '$location', MenuCtrl]);

function MenuCtrl(AuthService, $location) {
    var vm = this;

    vm.logout = function () {
        AuthService.logout().then(function (response) {
            $location.path('/login');
        }, function (response) {
        });
    }


}
