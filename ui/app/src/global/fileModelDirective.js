'use strict';

angular.module('uiApp').directive('fileModel', [function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('change', function(changeEvent){
                 var files = changeEvent.target.files;

                if (files.length) {
                    scope.$apply(function () {
                        scope.vm.model.file = files[0];
                    });
                }
            });
        }
    };
}]);
