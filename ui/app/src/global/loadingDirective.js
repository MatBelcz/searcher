angular.module('uiApp').directive('loading', Loading);

function Loading() {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="text-center padding-top-big">' +
        '<br><br><i class="fa fa-spinner fa-spin fa-5x fa-fw" aria-hidden="true"></i><br><br> ' +
        'Przetwarzanie...</div>',
        link: function (scope, element, attr) {
            scope.$watch(attr.loader, function (val) {
                if (val) {
                    $(element).show();
                } else {
                    $(element).hide();
                }
            });
        }
    };
}