#  application main configuration section
FLASK_SERVER_NAME = 'localhost:5000'
FLASK_DEBUG = False

#  sql alchemy configuration section
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:test@172.17.0.2:3306/searcher'
SQLALCHEMY_TRACK_MODIFICATIONS = False
CLEAN_AND_CREATE_DB = False

#  swagger configuration section
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False

#  celery configuration section
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_REGISTERED_TASKS_SRC = ('api.search',)
CELERY_TASKS_FILENAME = 'tasks'

#  jwt configuration
JWT_SECRET_KEY = '\x8c\x05?g\xbe\x81\xb8\xfa\x1b\x8e\xc1K5\xcd\x92'
