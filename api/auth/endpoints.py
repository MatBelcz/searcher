from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Resource
from app import api
from auth.auth import register_account, sign_in, revoke_current_auth_token
from auth.serializers import register_response_model, register_input_model

ns = api.namespace('auth', description='Endpoints connected user authorization')


@ns.route('/login')
class Login(Resource):
    @api.expect(register_input_model, validate=True)
    @api.marshal_with(register_response_model)
    def post(self):
        payload = request.json

        auth_token = sign_in(payload)

        return {'auth_token': auth_token}, 201


@ns.route('/logout')
class Logout(Resource):
    @jwt_required
    def post(self):
        try:
            revoke_current_auth_token()
        except KeyError:
            return {'Nieprawidłowy token'}, 500

        return {'message': 'Wylogowano'}, 200


@ns.route('/register')
class RegisterUser(Resource):
    @api.expect(register_input_model, validate=True)
    @api.marshal_with(register_response_model)
    def post(self):
        payload = request.json

        auth_token = register_account(payload)

        return {'auth_token': auth_token}, 201
