from flask_restplus import fields

from restplus import api

register_response_model = api.model('Registration response model', {
    'authToken': fields.String(required=True, attribute='auth_token'),
})

register_input_model = api.model('Registration request model', {
    'email': fields.String(required=True),
    'password': fields.String(required=True),
})
