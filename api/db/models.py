import bcrypt

from db.database import db


class User(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.String(254), nullable=False)
    password = db.Column(db.String(500), nullable=False)

    def __init__(self, email, password):
        self.email = email
        self.password = User.hash_password(password)

    @staticmethod
    def hash_password(password):
        return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())


class SearchTask(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    search_keys = db.Column(db.String(1000), nullable=True)
    status = db.Column(db.String(1000), nullable=False)
    task_id = db.Column(db.String(1000), nullable=False, unique=True)
    error_info = db.Column(db.String(5000), nullable=True)
    rows = db.relationship('SearchRow', backref='searchrow', lazy='dynamic')


class SearchRow(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    url = db.Column(db.Text, nullable=False)
    occurrences = db.Column(db.JSON(none_as_null=False))
    search_id = db.Column(db.BigInteger, db.ForeignKey('search_task.id'), nullable=False)
