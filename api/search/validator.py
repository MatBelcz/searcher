from flask_restplus import reqparse
from validators import ValidationFailure
from werkzeug.datastructures import FileStorage
import validators
from constants import ALLOWED_SEARCH_EXTENSIONS

search_post_args = reqparse.RequestParser()
search_post_args.add_argument('searchKeys', type=str, required=True, location='form')
search_post_args.add_argument('file', type=FileStorage, required=True, location='files')


def is_allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_SEARCH_EXTENSIONS


def validate_urls(urls):
    errors = []

    for url in urls:
        if isinstance(validators.url(url), ValidationFailure):
            errors.append('Nieprawdłowy URL: {0}'.format(url))

    return errors
