from celery_worker import celery, session, celery_app
from constants import CeleryTaskStatus
from search.service import set_search_task_failure, set_search_task_success, add_search_task
from utils.url_searcher import get_search_key_counts_for_urls


class SearchKeywordsTask(celery.Task):

    def on_success(self, retval, task_id, args, kwargs):
        with celery_app.app_context():
            set_search_task_success(session, task_id, retval)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        with celery_app.app_context():
            set_search_task_failure(session, task_id, exc)

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        session.remove()


@celery.task(base=SearchKeywordsTask, bind=True)
def search_keywords_in_urls(self, urls, search_keys):
    add_search_task(session, self.request.id, CeleryTaskStatus.STARTED, search_keys)

    response_data = get_search_key_counts_for_urls(search_keys, urls)

    return response_data
