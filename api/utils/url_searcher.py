from urllib.request import Request, urlopen
from bs4 import BeautifulSoup

NEW_LINE = '\n'
CARRIAGE_RETURN = '\r'
UTF_8_ENCODING = 'utf-8'
COMMA_SEARCH_KEYS_SEPARATOR = ','


def get_search_key_counts_for_urls(search_keys, urls):
    result = {}

    for url in urls:
        result[url] = __get_search_key_count_for_single_url(search_keys, url)

    return result


def __get_search_key_count_for_single_url(search_keys, url):
    result = []

    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    url_content = webpage.decode(UTF_8_ENCODING)

    url_content = ''.join(url_content)
    url_text = __extract_text_from_url_content(url_content)

    for search_key in search_keys.split(COMMA_SEARCH_KEYS_SEPARATOR):
        is_key_in_url_text = url_text.count(search_key.lower()) > 0
        item = {
            'name': search_key,
            'flag': is_key_in_url_text
        }

        result.append(item)

    return result


def __extract_text_from_url_content(url_content):
    soup = BeautifulSoup(url_content, 'html.parser')
    c = soup.findAll('script')

    for item in c:
        item.extract()

    body_text = soup.body(text=True)
    joined_body_text = ''.join(body_text)
    joined_body_text = joined_body_text.replace(NEW_LINE, ' ').replace(CARRIAGE_RETURN, '')

    return joined_body_text.lower()
