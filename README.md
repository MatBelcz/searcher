# Searcher

##Wymagania:
    python3.5, virtualenv, pip, grunt, grunt-cli, bower, npm

##Instalacja (API):

* W katalogu ($HOME)/searcher/api wpisujemy:
 * tworzymy venv'a: virtualenv venv
 * aktywujemy venva: source venv/bin/activate
 * instalujemy zależności: pip install -r requirements.txt
 * konfigurujemy settings.py (redis, mysql, itp)
 * uruchamiamy aplikację: python app.py
 * uruchamiamy celery workera: celery -A celery_worker.celery worker (opcjonalnie -l info/debug)
 * do adminki można tez uruchomić flower'a: flower --port=5555

##Instalacja (UI):

* W katalogu ($HOME)/searcher/ui wpisujemy:
 * (sudo) npm install
 * bower install lub sudo bower install --allow-root
 * URUCHOMIENIE - (sudo) grunt serve


##Dodatkowe info:
    separator urli - nowa linia,
    separator kluczy wyszukiwania: , - klucz1, klucz 2,klucz3
